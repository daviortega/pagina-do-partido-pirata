---
title: Partido Pirata Finlandês na disputa em 2019
date: 2019-04-02
description: "As eleições parlamentares na Finlândia ocorrerão no domingo, 14 de abril de 2019, e o Partido Pirata entrará com uma sólida chance de conseguir pelo menos um pirata eleito. Faltando apenas algumas semanas, aqui está uma atualização da campanha de Saga Nyrén, candidata pela primeira vez em Helsinque. Muito Obrigado Saga!"
image: "./images/PPFI-298x300.png"
slug: first
---

_*As eleições parlamentares na Finlândia ocorrerão no domingo, 14 de abril de 2019, e o Partido Pirata entrará com uma sólida chance de conseguir pelo menos um pirata eleito. Faltando apenas algumas semanas, aqui está uma atualização da campanha de Saga Nyrén, candidata pela primeira vez em Helsinque. Muito Obrigado Saga!*_

O Partido Pirata Finlandês comemorou seu aniversário de dez anos ano passado. Em abril agora, temos uma boa chance de finalmente conseguir nosso primeiro representante no parlamento. O caminho não foi fácil, muito menos rápido, mas foi necessário e todo esse trabalho árduo está começando a dar frutos.

Já nas eleições municipais de 2017, demos um grande passo assegurando um assento na câmara municipal da capital Helsinque, e outra em Jyväskylä. Em Espoo, a segunda maior cidade da Finlândia, perdemos por algumas dezenas de votos que foram para nossos aliados eleitorais, os liberais. O segundo lugar, no entanto, registrou um vice-conselheiro e muitos lugares nos diferentes comitês municipais em Espoo.

Ter assentos em governos municipais significa que nossas políticas estão sendo implementadas, experimentadas e testadas pela primeira vez. Nossa atual presidente, Petrus Pennanen, teve sucesso ao exigir maior abertura e transparência do Governo em Helsinque, através de reuniões de encontros ao vivo que costumavam ser fechadas ao público. Foi ótimo ver o quanto podemos alcançar quando nos sentamos à mesa e “partilhamos o mesmo pão e vinho”.

É por isso que todos do Partido Pirata estão muito animados e entusiasmados com as eleições que estão pra acontecer daqui a poucos dias. Com nossa aliança eleitoral em Helsinque com outros três partidos, temos uma probabilidade muito alta de conseguir um assento no parlamento. Com sete candidatos cuidadosamente selecionados em Helsinque, incluindo eu e nossa presidente, Petrus Pennanen, estamos dando o nosso melhor e não poupando energia nesta reta final, para que os eleitores saibam: é isso, agora é a hora!